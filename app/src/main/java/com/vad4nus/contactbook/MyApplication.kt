package com.vad4nus.contactbook

import android.app.Application
import com.vad4nus.contactbook.modules.loginModule
import com.vad4nus.contactbook.modules.mainModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApplication : Application(){

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MyApplication)
            modules(loginModule,mainModule)
        }
    }
}