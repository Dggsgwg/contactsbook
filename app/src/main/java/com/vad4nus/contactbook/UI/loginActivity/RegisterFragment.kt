package com.vad4nus.contactbook.UI.loginActivity

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.vad4nus.contactbook.R
import com.vad4nus.contactbook.UI.mainActivity.MainActivity
import com.vad4nus.contactbook.databinding.FragmentRegisterBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterFragment : Fragment() {

    var binding: FragmentRegisterBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val model by viewModel<LoginActivityViewModel>()

        binding?.registerEmailAddress?.setText(model.login.value)
        binding?.registerPassword?.setText(model.password.value)
        model.errorMsg.postValue("")

        binding?.registerEmailAddress?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                binding?.registerErrorText?.text = ""
                model.errorMsg.postValue("")
                model.login.value = s.toString()
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding?.registerPassword?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                binding?.registerErrorText?.text = ""
                model.errorMsg.postValue("")
                model.password.value = s.toString()
                Log.d("TAG", s.toString())
                Log.d("TAG", model.password.value.toString())
            }

            override fun afterTextChanged(s: Editable) {}
        })

        model.errorMsg.observe(viewLifecycleOwner) {
            binding?.registerErrorText?.text = model.errorMsg.value
        }

        model.password.observe(viewLifecycleOwner) {
            Log.d("TAG", binding?.registerPassword?.text.toString())
            binding?.registerButton?.isEnabled = (!model.password.value.isNullOrEmpty()
                    && !model.login.value.isNullOrEmpty())
        }

        binding?.registerButton?.setOnClickListener { v: View? ->
            if (model.onRegisterButtonPressed()) {
                startActivity(Intent(context, MainActivity::class.java))
                requireActivity().finish()
            }
        }

        binding?.goToLogin?.setOnClickListener { v: View? ->
            model.login.postValue("")
            model.password.postValue("")

            val fragment = LoginFragment()
            val transaction = requireActivity().supportFragmentManager.beginTransaction()

            with(transaction) {
                setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                replace(R.id.fragmentContainer, fragment)
                commit()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }
}