package com.vad4nus.contactbook.UI.loginActivity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.vad4nus.contactbook.R
import com.vad4nus.contactbook.UI.mainActivity.MainActivity
import com.vad4nus.contactbook.databinding.FragmentLoginBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : Fragment() {

    var binding: FragmentLoginBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val model by viewModel<LoginActivityViewModel>()

        binding?.loginEmailAddress?.setText(model.login.value)
        binding?.loginPassword?.setText(model.password.value)
        model.errorMsg.postValue("")

        binding?.loginEmailAddress?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                binding?.loginErrorText?.text = ""
                model.errorMsg.postValue("")
                model.login.postValue(s.toString())
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding?.loginPassword?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                binding?.loginErrorText?.text = ""
                model.errorMsg.postValue("")
                model.password.postValue(s.toString())
            }

            override fun afterTextChanged(s: Editable) {}
        })

        model.errorMsg.observe(viewLifecycleOwner) {
            binding?.loginErrorText?.text = model.errorMsg.value
        }

        binding?.loginButton?.setOnClickListener { v: View? ->
            if (model.onLoginButtonPressed()) {
                startActivity(Intent(context, MainActivity::class.java))
                requireActivity().finish()
            }
        }

        binding?.goToRegister?.setOnClickListener { v: View? ->
            model.login.postValue("")
            model.password.postValue("")

            val fragment = RegisterFragment()
            val transaction = requireActivity().supportFragmentManager.beginTransaction()

            with(transaction){
                setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                replace(R.id.fragmentContainer, fragment)
                commit()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }
}