package com.vad4nus.contactbook.UI.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.vad4nus.contactbook.R
import com.vad4nus.contactbook.core.entities.Contact
import com.vad4nus.contactbook.databinding.FragmentContactBinding


class ContactItemAdapter : RecyclerView.Adapter<ContactItemAdapter.ViewHolder>() {

    private val contactList: MutableList<Contact> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = FragmentContactBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val itemsViewModel = contactList[position]

        if(null != itemsViewModel.photo) {
            holder.contactPicture.setImageBitmap(itemsViewModel.photo)
        } else {
            holder.contactPicture.setImageResource(R.drawable.default_profile_picture)
        }
        holder.contactName.text = itemsViewModel.name
        holder.contactNumber.text = itemsViewModel.phoneNumber
    }

    override fun getItemCount(): Int = contactList.size

    fun setItems(contacts : MutableList<Contact>) {
        contactList.clear()
        contactList.addAll(contacts)
    }

    class ViewHolder(binding: FragmentContactBinding) : RecyclerView.ViewHolder(binding.root) {
        val contactPicture: ImageView = binding.contactPicture
        val contactName: TextView = binding.contactName
        val contactNumber: TextView = binding.contactNumber
    }
}