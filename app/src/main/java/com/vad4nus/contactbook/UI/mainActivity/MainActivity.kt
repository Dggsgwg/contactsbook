package com.vad4nus.contactbook.UI.mainActivity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.vad4nus.contactbook.UI.adapters.ContactItemAdapter
import com.vad4nus.contactbook.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

    private var binding: ActivityMainBinding? = null
    private val model by viewModel<MainActivityViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        supportActionBar?.hide()

        requestPermissions(arrayOf(android.Manifest.permission.READ_CONTACTS), 0)

        val transaction = supportFragmentManager.beginTransaction()
        with(transaction) {
            replace(binding?.fragmentContainer?.id as Int, FragmentContactList())
            commit()
        }

        model.getContacts()
    }
}