package com.vad4nus.contactbook.UI.loginActivity

import android.content.Context
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vad4nus.contactbook.core.entities.User
import com.vad4nus.contactbook.repository.Database

class LoginActivityViewModel(
    private val ctx: Context
    ) : ViewModel() {

    private var db = Database(ctx)
    private val errorEnterEmail = "Введите Email."
    private val errorWrongEmail = "Введите корректный Email."
    private val errorWrongPassword = "Пароль должен быть\nдлиннее восьми символов"
    private val errorGeneral = "Неправильный логин или пароль"

    var errorMsg = MutableLiveData<String>("")

    val login = MutableLiveData<String>("")

    val password = MutableLiveData<String>("")

    private fun validateLoginData(): Boolean {
        when {
            login.value.isNullOrEmpty() -> {
                errorMsg.postValue(errorEnterEmail)
                return false
            }
            !Patterns.EMAIL_ADDRESS.matcher(login.value).matches() -> {
                errorMsg.postValue(errorWrongEmail)
                return false
            }
            password.value.toString().length < 8 -> {
                errorMsg.postValue(errorWrongPassword)
                return false
            }

            else -> return true
        }
    }

    fun onRegisterButtonPressed(): Boolean {
        if (validateLoginData()) {
            db?.addUser(User(login.value.toString(), password.value.toString()))
            saveToPrefs()
            return true
        }

        return false
    }

    fun onLoginButtonPressed(): Boolean {
        if (validateLoginData()) {
            if(db?.validateUser(User(login.value.toString(), password.value.toString())) == true) {
                saveToPrefs()
                return true
            } else {
                errorMsg.postValue(errorGeneral)
            }
        }

        return false
    }

    private fun saveToPrefs() {
        val prefs =
            ctx.getSharedPreferences("com.vad4mus.contactbook", Context.MODE_PRIVATE)?.edit()

        prefs?.apply {
            putString("userLogin", login.value)
            putString("userPassword", password.value)
            apply()
        }
    }
}
