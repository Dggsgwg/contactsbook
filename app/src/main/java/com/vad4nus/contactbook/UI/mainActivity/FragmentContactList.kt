package com.vad4nus.contactbook.UI.mainActivity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.vad4nus.contactbook.UI.adapters.ContactItemAdapter
import com.vad4nus.contactbook.databinding.FragmentContactListBinding

class FragmentContactList  : Fragment() {

    private var binding : FragmentContactListBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentContactListBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val model = ViewModelProvider(requireActivity()).get(MainActivityViewModel::class.java)
        binding?.itemContainer?.layoutManager = LinearLayoutManager(requireContext())
        val adapter = ContactItemAdapter()
        binding?.itemContainer?.adapter = adapter

        model.contacts.observe(viewLifecycleOwner) {
            model.contacts.value?.let { it1 -> adapter.setItems(it1) }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }
}