package com.vad4nus.contactbook.UI.loginActivity

import android.os.Bundle
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.vad4nus.contactbook.R
import com.vad4nus.contactbook.core.entities.User
import com.vad4nus.contactbook.databinding.ActivityLoginBinding
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.getViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {

    private var binding: ActivityLoginBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        this.supportActionBar?.hide()

        val transaction = supportFragmentManager.beginTransaction()
        with(transaction) {
            binding?.fragmentContainer?.id?.let { replace(it, LoginFragment()) }
            commit()
        }
    }
}