package com.vad4nus.contactbook.UI.mainActivity

import android.content.ContentUris
import android.content.Context
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.ContactsContract
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vad4nus.contactbook.core.entities.Contact
import java.io.InputStream

class MainActivityViewModel(
    val ctx: Context
) : ViewModel() {

    var contacts: MutableLiveData<MutableList<Contact>> = MutableLiveData(arrayListOf())

    fun getContacts(): MutableList<Contact>? {
        if (contacts.value?.isNotEmpty() == true) {
            return contacts.value
        }
        contacts.value = getContactsFromMemory()
        return contacts.value
    }

    private fun getContactsFromMemory(): MutableList<Contact> {
        val cr = ctx.contentResolver
        val contacts: MutableList<Contact> = arrayListOf()
        val cursor = cr.query(
            ContactsContract.Contacts.CONTENT_URI,
            null, null, null, "display_name ASC"
        )

        if (cursor?.moveToFirst() == true) {
            do {
                var id = 0
                var name = ""
                var hasNumber = -1
                var phoneNumber: String? = ""

                with(cursor) {
                    val idColumn = getColumnIndex(ContactsContract.Contacts._ID)
                    val nameColumn = getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)
                    val hasNumberColumn = getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)
                    id = getInt(idColumn)
                    name = getString(nameColumn)
                    hasNumber = getInt(hasNumberColumn)
                }

                if (hasNumber > 0) {
                    val phoneCursor = ctx.contentResolver.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null
                    )

                    phoneCursor?.moveToFirst()

                    val numCol =
                        phoneCursor?.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                    phoneNumber = phoneCursor?.getString(numCol as Int)

                    phoneCursor?.close()
                }

                val uri: Uri =
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id.toLong())
                val input: InputStream? =
                    ContactsContract.Contacts.openContactPhotoInputStream(ctx.contentResolver, uri)
                val photo = BitmapFactory.decodeStream(input)

                contacts.add(Contact(name, photo, phoneNumber as String))
            } while (cursor.moveToNext())
        }

        return contacts
    }

/*    fun splitIntoGroups(_contacts: MutableList<Contact>): MutableList<MutableList<Contact>> {
        val contacts = _contacts
        val tempList: MutableList<Contact> = arrayListOf()
        val returnList: MutableList<MutableList<Contact>> = arrayListOf()
        var letter = contacts[0].name[0]

        while (contacts.isNotEmpty()) {
            if (contacts[0].name[0].equals(letter)) {
                tempList.add(contacts[0])
                contacts.remove(contacts[0])
            } else {
                val list = mutableListOf<Contact>()
                list.addAll(tempList)
                returnList.add(list)
                tempList.clear()
                letter = contacts[0].name[0]
                tempList.add(contacts[0])
                contacts.remove(contacts[0])
            }
        }

        return returnList
    }*/
}