package com.vad4nus.contactbook.core.entities

data class User(
    var login: String = "",
    var password: String = ""
)