package com.vad4nus.contactbook.core.entities

import android.graphics.Bitmap

data class Contact (
    var name : String = "",
    var photo : Bitmap? = null,
    var phoneNumber: String = ""
    )