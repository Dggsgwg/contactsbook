package com.vad4nus.contactbook.repository

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import com.vad4nus.contactbook.core.entities.User

class Database(context: Context?) : SQLiteOpenHelper(context, "Database.db", null, 1) {

    private val TABLE_NAME = "UserData"

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        sqLiteDatabase.execSQL("create table if not exists $TABLE_NAME (login TEXT, password TEXT)")
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, prevVersion: Int, newVersion: Int) {}
    fun validateUser(user: User): Boolean {
        var cursor: Cursor? = null
        val login : String
        val password : String

        try {
            cursor = this.readableDatabase.query(
                TABLE_NAME,
                arrayOf("login", "password"),
                "login =?",
                arrayOf(user.login),
                null,
                null,
                null
            )
        } catch (e: SQLiteException) {
            e.printStackTrace()
        }

        if(cursor?.moveToFirst() == true) {
            with(cursor) {
                login = getString(0)
                password = getString(1)
                close()
            }
        } else return false

        return login == user.login && password == user.password
    }

    fun addUser(user: User) {
        val values = ContentValues()

        values.put("login", user.login)
        values.put("password", user.password)
        writableDatabase.insert(TABLE_NAME, null, values)
    }
}
