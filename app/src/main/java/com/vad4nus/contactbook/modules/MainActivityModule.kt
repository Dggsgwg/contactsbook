package com.vad4nus.contactbook.modules

import com.vad4nus.contactbook.UI.mainActivity.MainActivityViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainModule = module {
    viewModel {
        MainActivityViewModel(get())
    }
}