package com.vad4nus.contactbook.modules

import com.vad4nus.contactbook.UI.loginActivity.LoginActivityViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val loginModule = module {
    viewModel () {
        LoginActivityViewModel(get())
    }
}